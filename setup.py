import setuptools

setuptools.setup(
    name="helper_funcs",
    version="0.0.4",
    description="A collection of helper functions",
    packages=setuptools.find_packages(),
    python_requires='>=3.10'
)