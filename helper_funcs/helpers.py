#### HELPER FUNCTIONS ####
# 
# Installed with pip install -e
# 

import os
import re
import dotenv

dotenv.load_dotenv(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        ".env"
    )
)

__SSH_CREDS_MD_DIRECTORY = os.environ.get("SSH_CREDS_MD_DIRECTORY")

def get_ssh_creds():
    '''Get SSH credentials from a special markdown file. Replace this with your custom implementation'''

    username = None
    password = None

    if not __SSH_CREDS_MD_DIRECTORY:
        raise ValueError("No ssh creds can be loaded")

    with open(__SSH_CREDS_MD_DIRECTORY, 'r') as ssh_creds_file:
        ssh_creds_file_raw = ssh_creds_file.read()
        
        match_result = re.search("\"username\" : \"(\S*)\",\n *\"password\" : \"(\S*)\"", ssh_creds_file_raw)
        
        username = match_result.group(1)
        password = match_result.group(2)

    return {
        "username": username,
        "password": password
    }

