import subprocess
import psutil
import atexit
import time


class dockersocketforwarder():
    '''A class to forward a remote docker socket to a host port'''
    
    __forwarding_process = None

    def __init__(self, ssh_user, ssh_pass, local_port, remote_ip):

        self.SSH_USERNAME = ssh_user
        self.SSH_PASSWORD = ssh_pass
        self.REMOTE_IP = remote_ip
        self.LOCAL_PORT = local_port

        self.ssh_socket_forward_cmd = f'sshpass -p {self.SSH_PASSWORD} ssh -L {self.LOCAL_PORT}:/var/run/docker.sock -N {self.SSH_USERNAME}@{self.REMOTE_IP}'

        atexit.register(self.__disconnect)
    
    def __connect(self) -> str:
        # Check if a connection is already active
        if self.__forwarding_process:
            print("WARNING: An active connection created by me already exists. Attempting to close it.", end="")
            self.__disconnect()
            print(" Done.")
        
        # Start forwarding
        self.__forwarding_process = subprocess.Popen(
            self.ssh_socket_forward_cmd.split(),
            shell=False
        )
        
        print(f"Forwarding Process ID: {self.__forwarding_process.pid}")

        # Wait for the ssh tunnel to stabilise
        print("Wait for the tunnel to connect")
        time.sleep(5)
        print("It should be connected by now...")

        return f'tcp://localhost:{self.LOCAL_PORT}'
    
    def __disconnect(self):
        # Stop forwarding
        if not self.__forwarding_process:
            raise ValueError("No ssh tunneling process created by me, that I know of, exists for me to kill")
        
        parent = psutil.Process(self.__forwarding_process.pid)
        print(f"Killing port forwarding (Process ID: {self.__forwarding_process.pid})")
        for child in parent.children(recursive=True):
            child.kill()
        parent.kill()
        self.__forwarding_process = None
        print("Port forwarding process should have been killed")
    
    def __enter__(self):
        '''Start the ssh tunnel and return the docker tcp url'''

        return self.__connect()

    def __exit__(self, exc_type, exc_val, exc_tb):
        '''Kill the stop forwarding process'''
        if self.__forwarding_process:
            self.__disconnect()
        else:
            print("No connection to disconnect")
    
    def manual_connect(self) :
        '''Connect manually'''
        return self.__connect()
    
    def manual_disconnect(self):
        '''Disconnect manually'''
        self.__disconnect()
        
        