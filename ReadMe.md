# Helper Funcs

A collection of custom functions to aid with development.

Keep in mind anything can and will change during development. I will try to keep this up to date.

## Installation

These functions are run and tested with python 3.11 under a linux environment. It should run on lower python versions, but I do not guarantee it can run in any other OS. I will try in the future to make it more generic.

I suggest to install it in your virtual environment in editable mode, so that it works without relational import problems. To do this run `pip install -e <directory_of_setup.py>` from your venv.

### Prerequisites

* Linus OS with installed packages:
    * sshpass
    * ssh

As of writing this, it does not require any external python modules.

## Functions

### helper_funcs.helpers.get_ssh_creds()

A function to load the ssh credentials from a local file. For my environment it suits me to fetch them from an already existing `.md` file using regex. The directory of this file is kept in a `.env` file in the `helpler_funcs/` directory (same as this `ReadMe.md`). This should be replaced with a custom implementation depending on your situation.

Example of the `.env` file contents:
```
SSH_CREDS_MD_DIRECTORY=/directory/to/ssh/credentials.md
```

Using the function:

```python
from helper_funcs.helpers import get_ssh_creds

ssh_creds = get_ssh_creds()

print(ssh_creds)
# {'username': 'user', 'password': 'pass'}
```

## Classes

### from helper_funcs.dockerSocketForwarder.dockersocketforwarder

A class to initiate an ssh tunnel to temporarily forward the docker socket from a remote server to a local tcp port. A reliable way I found to achieve this is by running the bash command:

```bash
sshpass -p {SSH_PASSWORD} ssh -L {LOCAL_PORT}:/var/run/docker.sock -N {SSH_USERNAME}@{REMOTE_IP}
```

And killing the process running this command to stop the ssh tunnel forwarding.

The module used to connect with the docker CLI is [docker-py](https://github.com/docker/docker-py)

Initialising the module:

```python
from helper_funcs.dockerSocketForwarder import dockersocketforwarder

ssh_forwarder = dockersocketforwarder(
    ssh_user=ssh_creds['username'],
    ssh_pass=ssh_creds['password'],
    remote_ip='192.168.1.1',
    local_port=4321
)
```


Using the module inside a `with` statement:

```python
with ssh_forwarder as DOCKER_TCP_URL:
    docker_client = docker.DockerClient(
        base_url=DOCKER_TCP_URL
    )

    print(docker_client.containers.list())
```

Manually starting and stopping a connection (in a jupyter notebook for example):

```python
# Start connection
DOCKER_TCP_URL = ssh_forwarder.manual_connect()

docker_client = docker.DockerClient(
    base_url=DOCKER_TCP_URL
)
print(docker_client.containers.list())

# Close connection
ssh_forwarder.manual_disconnect()
```

Remember to always close the connection. In the event that you forget, or something unexpected happens, the module *should* disconnect automatically at the exit of the program.

